package com.vatcalculator.view.activity;

import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import com.vatcalculator.R;
import com.vatcalculator.databinding.ActivityVatCalculationLayoutBinding;
import com.vatcalculator.model.response.VatCountryWiseRateModel;
import com.vatcalculator.model.response.VatResponseModel;
import com.vatcalculator.viewmodel.listener.VatManagementApiListener;
import com.vatcalculator.viewmodel.management.VatManagement;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ZAHID
 */
public class MainActivity extends AppCompatActivity implements VatManagementApiListener, CompoundButton.OnCheckedChangeListener {

    private static final String TAG = "MainActivity";
    private VatManagement vatManagement;
    private ActivityVatCalculationLayoutBinding layoutBinding;
    private VatResponseModel responseModel;
    private ArrayList<String> countryList;
    private ArrayAdapter<String> countryAdapter;
    private ArrayList<Double> ratesList;
    private double selectedRatePercent = 0.00;
    private DecimalFormat decimalFormat = new DecimalFormat("#.##");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layoutBinding = DataBindingUtil.setContentView(this, R.layout.activity_vat_calculation_layout);
        vatManagement = new VatManagement(this);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        countryList = new ArrayList<>();
        ratesList = new ArrayList<>();

        vatManagement.getVatDataInformation(this);
        layoutBinding.retryButton.setOnClickListener(v -> vatManagement.getVatDataInformation(MainActivity.this));
    }

    @Override
    public void startAPICall(String apiId) {
        layoutBinding.loadingLayout.setVisibility(View.VISIBLE);
        layoutBinding.mainLayout.setVisibility(View.GONE);
        layoutBinding.retryLayout.setVisibility(View.GONE);
    }

    @Override
    public void endAPICall(String apiId) {
        layoutBinding.loadingLayout.setVisibility(View.GONE);
        layoutBinding.mainLayout.setVisibility(View.GONE);
        layoutBinding.retryLayout.setVisibility(View.GONE);
    }

    @Override
    public void onVatDataSuccessResponse(String apiId, VatResponseModel model) {
        this.responseModel = model;
        layoutBinding.mainLayout.setVisibility(View.VISIBLE);

        for (VatCountryWiseRateModel countryWiseRateModel : model.getRates()) {
            countryList.add(countryWiseRateModel.getName());
        }

        if(countryAdapter==null)
            countryAdapter = new ArrayAdapter<String>(this, R.layout.adapter_string_list_layout, countryList);
        layoutBinding.countryList.setAdapter(countryAdapter);
        layoutBinding.countryList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ratesList.removeAll(ratesList);
                HashMap<String, Double> finalRateMap = vatManagement.getCurrentRates(responseModel.getRates().get(position).getPeriods());
                prepareRatesList(finalRateMap);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        layoutBinding.currency.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try{
                    Double amount;
                    if(TextUtils.isEmpty(s.toString())) amount = 0.00;
                    else amount = Double.parseDouble(s.toString());
                    layoutBinding.vatAmount.setText(String.valueOf(decimalFormat.format(amount * selectedRatePercent / 100)));
                    layoutBinding.totalAmount.setText(String.valueOf(decimalFormat.format(amount+(amount * selectedRatePercent / 100))));
                }catch (NumberFormatException e){}
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

    }

    @Override
    public void onVatDataFailResponse(String apiId, String errorMessage) {
        layoutBinding.retryLayout.setVisibility(View.VISIBLE);
        layoutBinding.errorMessage.setText(errorMessage);
    }

    private void prepareRatesList(HashMap<String, Double> rateMap){
        layoutBinding.vatTypes.removeAllViews();
        int position = 0;
        for (String key : rateMap.keySet()) {
            RadioButton rateButton = new RadioButton(this);
            rateButton.setText(key+" ("+rateMap.get(key)+"%)");
            rateButton.setId(position);
            rateButton.setOnCheckedChangeListener(this);
            ratesList.add(rateMap.get(key));

            if(position == 0) rateButton.setChecked(true);
            position++;
            layoutBinding.vatTypes.addView(rateButton);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked)
            selectedRatePercent = ratesList.get(buttonView.getId());
        layoutBinding.currency.setText(layoutBinding.currency.getText());
    }
}
