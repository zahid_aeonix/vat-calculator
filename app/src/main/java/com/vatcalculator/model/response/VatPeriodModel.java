package com.vatcalculator.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import java.io.Serializable;

public class VatPeriodModel implements Serializable {

    @SerializedName("effective_from")
    @Expose
    private String effectiveFrom;
    @SerializedName("rates")
    @Expose
    private Object rates;

    public String getEffectiveFrom() {
        return effectiveFrom;
    }

    public void setEffectiveFrom(String effectiveFrom) {
        this.effectiveFrom = effectiveFrom;
    }

    public Object getRates() {
        return rates;
    }

    public void setRates(Object rates) {
        this.rates = rates;
    }
}
