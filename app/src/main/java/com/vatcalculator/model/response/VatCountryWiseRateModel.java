package com.vatcalculator.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class VatCountryWiseRateModel implements Serializable {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("country_code")
    @Expose
    private String countryCode;
    @SerializedName("periods")
    @Expose
    private ArrayList<VatPeriodModel> periods;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public ArrayList<VatPeriodModel> getPeriods() {
        return periods;
    }

    public void setPeriods(ArrayList<VatPeriodModel> periods) {
        this.periods = periods;
    }
}
