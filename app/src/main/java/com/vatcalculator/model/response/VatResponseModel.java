package com.vatcalculator.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class VatResponseModel implements Serializable {

    @SerializedName("details")
    @Expose
    private String details;
    @SerializedName("rates")
    @Expose
    private ArrayList<VatCountryWiseRateModel> rates;

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public ArrayList<VatCountryWiseRateModel> getRates() {
        return rates;
    }

    public void setRates(ArrayList<VatCountryWiseRateModel> rates) {
        this.rates = rates;
    }
}
