package com.vatcalculator.model.datamodel;

/**
 * Created by ZAHID
 */
public class ResponseCode {

    public static final int SUCCESS_RESPONSE = 200;
    public static final int UNAUTHENTICATION = 401;
    public static final int INVALID_JSON_RESPONSE = 470;
    public static final int SERVER_ERROR = 500;
    public static final int UNKNOWN_ERROR = 480;
    public static final int INVALID_REQUEST = 495;
}
