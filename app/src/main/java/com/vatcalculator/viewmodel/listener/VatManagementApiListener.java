package com.vatcalculator.viewmodel.listener;

import com.vatcalculator.model.response.VatResponseModel;

/**
 * Created by ZAHID
 */
public interface VatManagementApiListener extends BaseApiListener{
    void onVatDataSuccessResponse(String apiId, VatResponseModel model);
    void onVatDataFailResponse(String apiId, String errorMessage);
}
