package com.vatcalculator.viewmodel.listener;

/**
 * Created by ZAHID
 */
public interface BaseApiListener {
    void startAPICall(String apiId);
    void endAPICall(String apiId);
}
