package com.vatcalculator.viewmodel.management;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.vatcalculator.model.datamodel.CommonUtil;
import com.vatcalculator.model.response.VatPeriodModel;
import com.vatcalculator.model.response.VatResponseModel;
import com.vatcalculator.viewmodel.listener.VatManagementApiListener;
import com.vatcalculator.viewmodel.network.ApiHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static android.content.ContentValues.TAG;
/**
 * Created by ZAHID
 */
public class VatManagement {

    ApiHandler apiHandler;
    public static final String VAT_INFORMATION_API_ID = "VAT_INFO_API";
    private VatManagementApiListener vatManagementApiListener;

    //private String SAMPLE_DATA = "{\"details\":\"http://github.com/adamcooke/vat-rates\",\"version\":null,\"rates\":[{\"name\":\"Spain\",\"code\":\"ES\",\"country_code\":\"ES\",\"periods\":[{\"effective_from\":\"0000-01-01\",\"rates\":{\"super_reduced\":4,\"reduced\":10,\"standard\":21}}]},{\"name\":\"Bulgaria\",\"code\":\"BG\",\"country_code\":\"BG\",\"periods\":[{\"effective_from\":\"0000-01-01\",\"rates\":{\"reduced\":9,\"standard\":20}}]},{\"name\":\"Hungary\",\"code\":\"HU\",\"country_code\":\"HU\",\"periods\":[{\"effective_from\":\"0000-01-01\",\"rates\":{\"reduced1\":5,\"reduced2\":18,\"standard\":27}}]},{\"name\":\"Latvia\",\"code\":\"LV\",\"country_code\":\"LV\",\"periods\":[{\"effective_from\":\"0000-01-01\",\"rates\":{\"reduced\":12,\"standard\":21}}]},{\"name\":\"Poland\",\"code\":\"PL\",\"country_code\":\"PL\",\"periods\":[{\"effective_from\":\"0000-01-01\",\"rates\":{\"reduced1\":5,\"reduced2\":8,\"standard\":23}}]},{\"name\":\"United Kingdom\",\"code\":\"UK\",\"country_code\":\"GB\",\"periods\":[{\"effective_from\":\"2011-01-04\",\"rates\":{\"standard\":20,\"reduced\":5}}]},{\"name\":\"Czech Republic\",\"code\":\"CZ\",\"country_code\":\"CZ\",\"periods\":[{\"effective_from\":\"0000-01-01\",\"rates\":{\"reduced\":15,\"standard\":21}}]},{\"name\":\"Malta\",\"code\":\"MT\",\"country_code\":\"MT\",\"periods\":[{\"effective_from\":\"0000-01-01\",\"rates\":{\"reduced1\":5,\"reduced2\":7,\"standard\":18}}]},{\"name\":\"Italy\",\"code\":\"IT\",\"country_code\":\"IT\",\"periods\":[{\"effective_from\":\"0000-01-01\",\"rates\":{\"super_reduced\":4,\"reduced\":10,\"standard\":22}}]},{\"name\":\"Slovenia\",\"code\":\"SI\",\"country_code\":\"SI\",\"periods\":[{\"effective_from\":\"0000-01-01\",\"rates\":{\"reduced\":9.5,\"standard\":22}}]},{\"name\":\"Ireland\",\"code\":\"IE\",\"country_code\":\"IE\",\"periods\":[{\"effective_from\":\"0000-01-01\",\"rates\":{\"super_reduced\":4.8,\"reduced1\":9,\"reduced2\":13.5,\"standard\":23,\"parking\":13.5}}]},{\"name\":\"Sweden\",\"code\":\"SE\",\"country_code\":\"SE\",\"periods\":[{\"effective_from\":\"0000-01-01\",\"rates\":{\"reduced1\":6,\"reduced2\":12,\"standard\":25}}]},{\"name\":\"Denmark\",\"code\":\"DK\",\"country_code\":\"DK\",\"periods\":[{\"effective_from\":\"0000-01-01\",\"rates\":{\"standard\":25}}]},{\"name\":\"Finland\",\"code\":\"FI\",\"country_code\":\"FI\",\"periods\":[{\"effective_from\":\"0000-01-01\",\"rates\":{\"reduced1\":10,\"reduced2\":14,\"standard\":24}}]},{\"name\":\"Cyprus\",\"code\":\"CY\",\"country_code\":\"CY\",\"periods\":[{\"effective_from\":\"0000-01-01\",\"rates\":{\"reduced1\":5,\"reduced2\":9,\"standard\":19}}]},{\"name\":\"Luxembourg\",\"code\":\"LU\",\"country_code\":\"LU\",\"periods\":[{\"effective_from\":\"2016-01-01\",\"rates\":{\"super_reduced\":3,\"reduced1\":8,\"standard\":17,\"parking\":13}},{\"effective_from\":\"2015-01-01\",\"rates\":{\"super_reduced\":3,\"reduced1\":8,\"reduced2\":14,\"standard\":17,\"parking\":12}},{\"effective_from\":\"0000-01-01\",\"rates\":{\"super_reduced\":3,\"reduced1\":6,\"reduced2\":12,\"standard\":15,\"parking\":12}}]},{\"name\":\"Romania\",\"code\":\"RO\",\"country_code\":\"RO\",\"periods\":[{\"effective_from\":\"2017-01-01\",\"rates\":{\"reduced1\":5,\"reduced2\":9,\"standard\":19}},{\"effective_from\":\"2016-01-01\",\"rates\":{\"reduced1\":5,\"reduced2\":9,\"standard\":20}},{\"effective_from\":\"0000-01-01\",\"rates\":{\"reduced1\":5,\"reduced2\":9,\"standard\":24}}]},{\"name\":\"Estonia\",\"code\":\"EE\",\"country_code\":\"EE\",\"periods\":[{\"effective_from\":\"0000-01-01\",\"rates\":{\"reduced\":9,\"standard\":20}}]},{\"name\":\"Greece\",\"code\":\"EL\",\"country_code\":\"GR\",\"periods\":[{\"effective_from\":\"2016-06-01\",\"rates\":{\"reduced1\":6,\"reduced2\":13.5,\"standard\":24}},{\"effective_from\":\"2016-01-01\",\"rates\":{\"reduced1\":6,\"reduced2\":13.5,\"standard\":23}},{\"effective_from\":\"0000-01-01\",\"rates\":{\"reduced1\":6.5,\"reduced2\":13,\"standard\":23}}]},{\"name\":\"Lithuania\",\"code\":\"LT\",\"country_code\":\"LT\",\"periods\":[{\"effective_from\":\"0000-01-01\",\"rates\":{\"reduced1\":5,\"reduced2\":9,\"standard\":21}}]},{\"name\":\"France\",\"code\":\"FR\",\"country_code\":\"FR\",\"periods\":[{\"effective_from\":\"2014-01-01\",\"rates\":{\"super_reduced\":2.1,\"reduced1\":5.5,\"reduced2\":10,\"standard\":20}},{\"effective_from\":\"2012-01-01\",\"rates\":{\"super_reduced\":2.1,\"reduced1\":5.5,\"reduced2\":7,\"standard\":19.6}},{\"effective_from\":\"0000-01-01\",\"rates\":{\"super_reduced\":2.1,\"reduced1\":5.5,\"standard\":19.6}}]},{\"name\":\"Croatia\",\"code\":\"HR\",\"country_code\":\"HR\",\"periods\":[{\"effective_from\":\"0000-01-01\",\"rates\":{\"reduced1\":5,\"reduced2\":13,\"standard\":25}}]},{\"name\":\"Belgium\",\"code\":\"BE\",\"country_code\":\"BE\",\"periods\":[{\"effective_from\":\"0000-01-01\",\"rates\":{\"reduced1\":6,\"reduced2\":12,\"standard\":21,\"parking\":12}}]},{\"name\":\"Netherlands\",\"code\":\"NL\",\"country_code\":\"NL\",\"periods\":[{\"effective_from\":\"2012-10-01\",\"rates\":{\"reduced\":6,\"standard\":21}},{\"effective_from\":\"0000-01-01\",\"rates\":{\"reduced\":6,\"standard\":19}}]},{\"name\":\"Slovakia\",\"code\":\"SK\",\"country_code\":\"SK\",\"periods\":[{\"effective_from\":\"0000-01-01\",\"rates\":{\"reduced\":10,\"standard\":20}}]},{\"name\":\"Germany\",\"code\":\"DE\",\"country_code\":\"DE\",\"periods\":[{\"effective_from\":\"0000-01-01\",\"rates\":{\"reduced\":7,\"standard\":19}}]},{\"name\":\"Portugal\",\"code\":\"PT\",\"country_code\":\"PT\",\"periods\":[{\"effective_from\":\"0000-01-01\",\"rates\":{\"reduced1\":6,\"reduced2\":13,\"standard\":23,\"parking\":13}}]},{\"name\":\"Austria\",\"code\":\"AT\",\"country_code\":\"AT\",\"periods\":[{\"effective_from\":\"2016-01-01\",\"rates\":{\"reduced1\":10,\"reduced2\":13,\"standard\":20,\"parking\":13}},{\"effective_from\":\"0000-01-01\",\"rates\":{\"reduced\":10,\"standard\":20,\"parking\":12}}]}]}";

    public VatManagement(Context context){
        apiHandler = new ApiHandler(context) {
            @Override
            public void startApiCall(String requestId) {
                if (requestId.equals(VAT_INFORMATION_API_ID) && vatManagementApiListener!=null)
                    vatManagementApiListener.startAPICall(requestId);
            }

            @Override
            public void endApiCall(String requestId) {
                if (requestId.equals(VAT_INFORMATION_API_ID) && vatManagementApiListener!=null)
                    vatManagementApiListener.endAPICall(requestId);
            }

            @Override
            public void successResponse(String requestId, Map hashMap, JSONObject response) {
                Gson gson = new Gson();
                if (requestId.equals(VAT_INFORMATION_API_ID) && vatManagementApiListener!=null)
                    vatManagementApiListener.onVatDataSuccessResponse(requestId, gson.fromJson(response.toString(),VatResponseModel.class));
            }

            @Override
            public void failResponse(String requestId, int responseCode, String message) {
                if (requestId.equals(VAT_INFORMATION_API_ID) && vatManagementApiListener!=null)
                    vatManagementApiListener.onVatDataFailResponse(requestId, message);
            }
        };
    }

    public void getVatDataInformation(VatManagementApiListener vatManagementApiListener){
        this.vatManagementApiListener = vatManagementApiListener;
        //vatManagementApiListener.onVatDataSuccessResponse(VAT_INFORMATION_API_ID, new Gson().fromJson(SAMPLE_DATA,VatResponseModel.class));
        apiHandler.httpRequest(CommonUtil.BASE_URL,"","get", VAT_INFORMATION_API_ID, new HashMap());
    }

    public HashMap<String,Double> getCurrentRates(ArrayList<VatPeriodModel> periodList){
        HashMap<String, Double> ratesMap = new HashMap<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        VatPeriodModel currentPeriod = periodList.get(0);
        for(int x=1;x<periodList.size();x++){
            try{
                Date currentPeriodDate = dateFormat.parse(currentPeriod.getEffectiveFrom());
                Date newPeriodDate = dateFormat.parse(periodList.get(x).getEffectiveFrom());
                if(currentPeriodDate.before(newPeriodDate)){
                    currentPeriod = periodList.get(x);
                }
            }catch (ParseException e){e.getStackTrace();}
        }

        try {
            JSONObject currentRateJsonObject = new JSONObject(currentPeriod.getRates().toString());
            Iterator<String> ratesIterator = currentRateJsonObject.keys();
            while(ratesIterator.hasNext()) {
                String key = ratesIterator.next();
                ratesMap.put(key, currentRateJsonObject.getDouble(key));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e(TAG, "getCurrentRates: "+currentPeriod.getEffectiveFrom());

        return ratesMap;
    }

}
