package com.vatcalculator.viewmodel.network;

import android.content.Context;

import com.vatcalculator.R;
import com.vatcalculator.model.datamodel.ResponseCode;

import org.json.JSONObject;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.HttpException;
import retrofit2.Response;

/**
 * Created by ZAHID
 */
public abstract class ApiHandler {

    private Context context;

    public ApiHandler(Context context) {
        this.context = context;
    }

    public void httpRequest(String baseUrl, String path, String requestType, final String requestId, Map hashMap) {
        try {
            startApiCall(requestId);
            Call<ResponseBody> bodyToCall = null;
            if(hashMap== null) {
                failResponse(requestId, ResponseCode.INVALID_REQUEST, context.getString(R.string.invalid_request));
                return;
            }

            if (requestType.toLowerCase().equals("get"))
                bodyToCall = ApiClient.callRetrofit(baseUrl).getRequest(path, hashMap);
            else if (requestType.toLowerCase().equals("post"))
                bodyToCall = ApiClient.callRetrofit(baseUrl).postRequest(path, hashMap);
            else if (requestType.toLowerCase().equals("img"))
                bodyToCall = ApiClient.callRetrofit(baseUrl).sendDocuments(path, hashMap);
            bodyToCall.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    endApiCall(requestId);
                    if (response.code() == ResponseCode.SUCCESS_RESPONSE) {
                        try {
                            String responseString = response.body().string();
                            JSONObject jsonObject = new JSONObject(responseString);
                            successResponse(requestId, hashMap, jsonObject);
                        } catch (Exception e) {
                            failResponse(requestId, ResponseCode.INVALID_JSON_RESPONSE, context.getString(R.string.invalid_json_response));
                        }
                    } else {
                        failResponse(requestId, ResponseCode.UNAUTHENTICATION, context.getString(R.string.network_error));
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                    endApiCall(requestId);
                    if (throwable instanceof HttpException)
                        failResponse(requestId, ResponseCode.SERVER_ERROR, context.getString(R.string.invalid_request));
                    else if (throwable instanceof UnknownHostException)
                        failResponse(requestId, ResponseCode.SERVER_ERROR, context.getString(R.string.connectivity_error));
                    else if (throwable instanceof IOException)
                        failResponse(requestId, ResponseCode.SERVER_ERROR, context.getString(R.string.internet_not_connected));
                    else
                        failResponse(requestId, ResponseCode.SERVER_ERROR, context.getString(R.string.unknown_error));
                }
            });
        } catch (Exception e) {
            endApiCall(requestId);
            failResponse(requestId, ResponseCode.UNKNOWN_ERROR, (e.getMessage() == null ? context.getString(R.string.unknown_error) : e.getMessage()));
        }
    }

    public abstract void startApiCall(String requestId);
    public abstract void endApiCall(String requestId);
    public abstract void successResponse(String requestId, Map hashMap, JSONObject response);
    public abstract void failResponse(String requestId, int responseCode, String message);
}
